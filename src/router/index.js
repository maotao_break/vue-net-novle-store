import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from "@/views/HomePage";
import RecommendPage from "@/views/RecommendPage";
import ExplorePage from "@/views/ExplorePage";
import FreeBookPage from "@/views/FreeBookPage";
import RankPage from "@/views/RankPage";
import SearchPage from "@/views/SearchPage";
import FullBookPage from "@/views/FullBookPage";
import InfoOptionPage from "@/views/InfoOptionPage";
import BookshelfComponent from "@/components/content/BookshelfComponent";
import UserOptionComponent from "@/components/option/UserOptionComponent";
import HistoryComponent from "@/components/history/HistoryComponent";
import ExploreComponent from "@/components/explore/ExploreComponent";
import LoginPage from "@/views/LoginPage";
import RegisterPage from "@/views/RegisterPage";
import ChapterPage from "@/views/ChapterPage";
import MessageComponent from "@/components/pilot/message/MessageComponent";
import TransferPage from "@/views/TransferPage";
import ManagePage from "@/views/ManagePage";
import ManageUserComponent from "@/components/manage/user/ManageUserComponent";
import ManageBookComponent from "@/components/manage/book/ManageBookComponent";
import {Message} from "element-ui";
import store from "@/store";
import {countBookList, login} from "@/utils/api";
Vue.use(VueRouter)



const routes = [
  {
    path: '/test',
    name: 'test',
    component: MessageComponent
  },
  {
    path: '/',
    name: 'LoginPage',
    component: LoginPage
  },
  {
    path: '/TransferPage',
    name: 'TransferPage',
    component: TransferPage
  },
  {
    path: '/Register',
    name: 'RegisterPage.vue',
    component: RegisterPage
  },


  {
    path: '/ChapterPage',
    name: '/ChapterPage',

    component: ChapterPage
  },
  {
    path: '/Login',
    name: 'LoginPage',
    component: LoginPage
  },
  {
    path: '/HomePage',
    name: 'HomePage',
    component: HomePage
  },
  {
    //精选
    path: '/RecommendPage',
    name: 'RecommendPage',
    component: RecommendPage
  },
  {
    path: '/ManagePage',
    name: 'ManagePage',
    component: ManagePage,
    children: [
      {
        path: '/ManageUserComponent',
        name: 'ManageUserComponent',
        component: ManageUserComponent
      },
      {
        path: '/ManageBookComponent',
        name: 'ManageBookComponent',
        component: ManageBookComponent
      }
    ]
  },
  {
    //发现
    path: '/ExplorePage',
    name: 'ExplorePage',
    component: ExplorePage,
    children: [
      {
        path: 'FreeBookPage',
        name: 'FreeBookPage',
        component: FreeBookPage
      },
      {
        path: 'FullBookPage',
        name: 'FullBookPage',
        component: FullBookPage
      },
      {
        path: 'RankPage',
        name: 'RankPage',
        component: RankPage
      },
      {
        path: 'ExploreComponent',
        name: 'ExploreComponent',
        component: ExploreComponent
      },
    ]
  },

  {
    path: '/SearchPage',
    name: 'SearchPage',
    component: SearchPage
  }
  ,
  {
    path: '/InfoOptionPage',
    name: 'InfoOptionPage',
    component: InfoOptionPage,
    children:[
      {
        path: 'BookshelfComponent',
        name: 'BookshelfComponent',
        component: BookshelfComponent
      },
      {
        path: 'UserOptionComponent',
        name: 'UserOptionComponent',
        component: UserOptionComponent
      },
      {
        path: 'HistoryComponent',
        name: 'HistoryComponent',
        component: HistoryComponent
      },
    ]
  }
]

const router = new VueRouter({
  routes
})
/**
 * @param {to} 将要去的路由
 * @param {from} 出发的路由
 * @param {next} 执行下一步
 */

router.beforeResolve((to, from, next) => {
  if(window.localStorage.getItem('token') && window.localStorage.getItem('user')){
    countBookList()
    if(to.path == '/')
      next({
        path: '/HomePage'
      })
    else
      next()
  }else{
    if(to.path == '/' || to.path == '/Login' || to.path == '/Register'){
      next()
    }else if(to.path === '/ManageBookComponent'){
      const userType = store.state.user.userType
      if(userType ==="admin"){
        next()
      }else{
        next(to.redirectedFrom)
      }
    } else {
      next({
        path: '/Login'
      })
    }
  }


})

// // 解决导航栏中的vue-router在3.0版本以上重复点菜单报错问题
// const originalPush = VueRouter.prototype.push
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }
export default router
