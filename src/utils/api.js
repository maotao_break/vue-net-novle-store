import {post as post_from, get as get_from, post_json, del, del_json, put_json, patch} from './axios/axios'
import axios from "axios";
export const login = p => post_from('/User/login', p);
export const delUserByNumberList = p => del_json('/User/delUserByNumberList',p);
export const getUserList = p => get_from('/User/getUserList',p);
export const registerUser = p => put_json('/User/register',p);
export const searchUserByInfo = p => get_from('/User/searchUserByInfo',p);
export const isExistByUserName = p => get_from('/User/isExistByUserName',p);
export const updateUser = p => patch('/User/updateUser',p);


export const addBook = p => put_json('/Book/addBook', p);
export const countBookList = p => get_from('/Book/countBookList', p);
export const delBooksByNumberList = p => del_json('/Book/delBooksByNumberList', p);
export const getBookList = p => get_from('/Book/getBookList', p);
export const getBookListByFree = p => get_from('/Book/getBookListByFree',p);
export const getBookListByFull = p => get_from('/Book/getBookListByFull',p);
export const getBookListByHot = p => get_from('/Book/getBookListByHot',p);
export const getBookListByType = p => get_from('/Book/getBookListByType',p);
export const getCountBySearchBookByType = p => get_from('/Book/getCountBySearchBookByType',p);
export const getChapterTotal = p => get_from('/Book/getChapterTotal',p);
export const getCountSearchBook = p => get_from('/Book/getCountSearchBook',p);
export const readBook = p => get_from('/Book/readBook',p);
export const searchBook = p => get_from('/Book/searchBook',p);
export const getBookListByNew = p => get_from('/Book/getBookListByNew',p);
export const setQuantityByNumber = p => get_from('/Book/setQuantityByNumber',p);
export const setBookByNumber = p => patch('/Book/setBookByNumber',p);
export const clearCache = p => post_from('/Event/clearCache',p);



// export const setPassword = p => post_json('/User/updateUser', p);
// export const setNickName = p => post_json('/User/updateUser', p);
// export const setUserByNumber = p => post_json('/User/updateUser',p);
