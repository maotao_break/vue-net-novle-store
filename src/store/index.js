import Vue from 'vue'
import Vuex from 'vuex'
import Cookies from 'js-cookie'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
        count: 0,
        user:Object,
        searchText : String


  },
  getters: {
      getUser: (state) => {
          state.user = JSON.parse(localStorage.getItem('user'))
          return state.user;
      },
      getSearchText:(state)=>{

          return state.user;
      }

  },
  mutations: {
      setUser: (state, user) => {
          localStorage.setItem('user',JSON.stringify(user));
          state.user = user;
      },
      setSearchText: (state, searchText) => {
          state.user = searchText;
      },
  },
  actions: {
  },
  modules: {
  }
})
