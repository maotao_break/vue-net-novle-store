# vue-NetNovelStore

# 项目演示
```
http://139.9.50.22/#/
```

## 项目部署
```
npm install

npm install vue

npm install element-ui -S

npm install --save vue-monoplasty-slide-verify

```
### 项目配置
在 vue.config.js 里配置后端地址即可
### 运行项目
```
npm run serve
```

### 项目打包
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

