const { defineConfig } = require('@vue/cli-service')

// const baseURL = "http://127.0.0.1:7000/"
const baseURL = "http://127.0.0.1:8080/"
// const baseURL = "http://139.9.50.22:7000/"
module.exports = defineConfig({
  publicPath: './',   //修复打包后页面空白
  transpileDependencies: true,
  devServer:{
    //webpack-dev-server配置
    port : 8081, //配置本项目运行端口
    proxy: {                //配置代理服务器来解决跨域问题
      '/api': {
        target: baseURL,      //配置要替换的后台接口地址
        changOrigin: true,                      //配置允许改变Origin
        pathRewrite: {
          '^/api': ''
        }
      },
    },

  },
})
